#include <stdio.h>
#define SUM 1000  /* sum borrowed */
#define RATE 0.03 /* interest rate */
#define NMONTHS 25 /* loan duration */

/* Lab 2 of FEEG6002 */
/* Use for loops to compute compound interest on a debt */
int main(int argc, char *argv[])
{
  float s, debt, rate, interest, total_interest;
  int month;
  s = SUM;
  rate = RATE;
  debt = s;
  total_interest = 0;
  for (month = 1; month < NMONTHS; month++) {
    interest = debt * rate;
    total_interest += interest;
    debt = debt + interest;
    printf("month %2d: debt=%7.2f, interest=%4.2f, total_interest=%7.2f, "\
	   "frac=%6.2f%%\n", month, debt, interest, total_interest,
	   100 * total_interest / s);
    }
  return 0;
}
