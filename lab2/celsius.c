#include <stdio.h>

#define CMIN -30
#define CMAX 30
int main() {
  int celsius;
  float fahrenheit;

  for (celsius=CMIN; celsius<=CMAX; celsius+=2) {
    fahrenheit = celsius * 9 / 5.0 + 32;
    printf("%3d = %5.1f\n", celsius, fahrenheit);
  }
  return 0;
}
