#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* #define XMIN 1.0 */
/* #define XMAX 10.0 */
/* #define N 10 */


int main(int argc, char *argv[])
{
  float XMIN, XMAX, dx, x;
  int N;

  if (argc == 4) {
    XMIN = atof(argv[1]);
    XMAX = atof(argv[2]);
    N = atoi(argv[3]);
    /* printf("Parameters: xmin=%f, xmax=%f, N=%d\n", XMIN, XMAX, N); */
  }
  else {
    printf("Usage: \t tabulate xmin xmax N\n");
    return 1;
  };
  /* printf("Computing the table\n"); */
  dx = (XMAX - XMIN) / (N-1);
  for (int i=0; i<N; ++i) {
    x = XMIN + i * dx;
    printf("%f %f\n", x, sin(x));
  }
  return argc;

}
