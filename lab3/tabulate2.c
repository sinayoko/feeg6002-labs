#include <stdio.h>
#include <math.h>

#define XMIN 1
#define XMAX 10
#define N 10

int main () {
  double dx, x;
  int i;
  dx = (XMAX - XMIN) / (double) (N-1);
  for (i=0; i<N; ++i) {
    x = XMIN + i * dx;
    printf("%f %f %f\n", x, sin(x), cos(x));
  }
  return 0;
}
