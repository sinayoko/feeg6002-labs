#include <stdio.h>
#include <math.h>

#define XMIN 1
#define XMAX 10
#define N 10

int main () {
  double dx, x;
  int i;
  /* The 1.0 is crucial here: not good! */
  dx = (XMAX - XMIN) / (double) (N-1.0);
  for (i=0; i<N; i++) {
    x = XMIN + i * dx;
    printf("%f %f\n", x, sin(x));
  }
  return 0;
}
