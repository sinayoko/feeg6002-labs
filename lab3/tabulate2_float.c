#include <stdio.h>
#include <math.h>

#define XMIN 1
#define XMAX 10
#define N 20

int main () {
  float dx, x;
  int i;
  dx = (XMAX - XMIN) / (float) (N-1);
  for (i=0; i<N; ++i) {
    x = XMIN + i * dx;
    printf("%f %f %f\n", x, sinf(x), cosf(x));
  }
  return 0;
}
